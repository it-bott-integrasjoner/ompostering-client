# ompostering-client

ompostering-client is a work in progress, and is based on dfo-sap-client.
It's a client for fetching, deleting and creating data in the omposterings solution.

## NOTE

The API returns all lowercase for GET, while the POST requires all uppercase
This is handled by adding ```alias_generator = to_upper_case``` to the model config,
which adds an uppercase alias to all members.
Having either camelcase or snake_case in the model members won't work well with the API.

## Usage

```python
from ompostering_client import OmposteringClient

c = OmposteringClient('https://test.app.uib.no/apxnyutvut/ompostering/ompostering/',
              begrepsverdier_url='begrepsverdier_api/',
              firma_url='firma_api/',
              tokens={'begrepsverdier_api': {'X-Gravitee-API-Key': 'c-d-a-b'},
                      'firma_api': {'X-Gravitee-API-Key': 'a-b-c-d'}})

response = c.get_begrepsverdier("72", "AH", "999")

response[0].attributevalue
```

## Tests

### Unit tests

```sh
python -m pytest
```

### Coverage

```sh
coverage run --branch --source=ompostering_client -m pytest && coverage report --show-missing
```

### Integration tests

Set credentials in `config.json` and run

```sh
python -m pytest -m integration
```
