"""Client for connecting to Ompostering API"""
import json
import logging
import typing
from types import ModuleType
import urllib.parse
from datetime import datetime, date
from typing import Any, Dict, List, Optional, Sequence, Type, Union

import requests

from ompostering_client.models import (
    Arbeidsordre,
    BaseModel_T,
    Begrepsverdi,
    Bilagstype,
    Bruker,
    Firma,
    OmpHode,
    OmpHodeError,
    Periode,
    Prosjekt,
    Konteringsregler,
    Ressurs,
    SetraBatch,
    TransaksjonHode,
    TransaksjonLinje,
    TransaksjonHodeError,
    Konto,
    Kontoer,
    Avgiftskode,
    AvgiftskodeListe,
)

logger = logging.getLogger(__name__)

JsonType = Any


def json_serializer(obj: Any) -> Optional[Union[str, ValueError]]:
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime, date)):
        return obj.isoformat(timespec="seconds")  # type: ignore[call-arg]
    return ValueError(f"Could not serialize object type: {type(obj)}")


def quote_path_arg(arg: Sequence[str]) -> str:
    return urllib.parse.quote_plus(str(arg))


def merge_dicts(*dicts: Optional[Dict[str, Any]]) -> Dict[str, Any]:
    """
    Combine a series of dicts without mutating any of them.

    >>> merge_dicts({'a': 1}, {'b': 2})
    {'a': 1, 'b': 2}
    >>> merge_dicts({'a': 1}, {'a': 2})
    {'a': 2}
    >>> merge_dicts(None, None, None)
    {}
    """
    combined = dict()
    for d in dicts:
        if not d:
            continue
        for k in d:
            combined[k] = d[k]
    return combined


class OmposteringEndpoints:
    def __init__(
        self,
        url: str,
        begrepsverdier_url: Optional[str] = "begrepsverdier_api/",
        bilagstyper_url: Optional[str] = "bilagstyper_api/",
        firma_url: Optional[str] = "firma_api/",
        prosjekt_url: Optional[str] = "prosjekt_api/",
        prosjekt_batch_url: Optional[str] = "prosjekt_batch_api/",
        konteringsregler_url: Optional[str] = "konteringsregler_api/",
        periode_url: Optional[str] = "perioder_api/",
        arbeidsordre_url: Optional[str] = "arbeidsordre_api/",
        arbeidsordre_batch_url: Optional[str] = "arbeidsordre_batch_api/",
        begrepsverdier_batch_url: Optional[str] = "begrepsverdier_batch_api/",
        omp_hode_url: Optional[str] = "omp_hode_api/",
        omp_hode_res_url: Optional[str] = "omp_hode_res_api/",
        bruker_url: Optional[str] = "bruker_api/",
        brukere_batch_url: Optional[str] = "brukere_batch_api/",
        transaksjon_hode_url: Optional[str] = "transaksjon_hode_api/",
        transaksjon_linje_url: Optional[str] = "transaksjon_linje_api/",
        kontoplaner_url: Optional[str] = "kontoplaner_api/",
        avgiftskoder_url: Optional[str] = "avgiftskoder_api/",
        ressurs_url: Optional[str] = "ressurs_api/",
    ):
        self.baseurl = url
        self.begrepsverdier_url = begrepsverdier_url
        self.bilagstyper_url = bilagstyper_url
        self.firma_url = firma_url
        self.prosjekt_url = prosjekt_url
        self.prosjekt_batch_url = prosjekt_batch_url
        self.konteringsregler_url = konteringsregler_url
        self.periode_url = periode_url
        self.arbeidsordre_url = arbeidsordre_url
        self.omp_hode_url = omp_hode_url
        self.omp_hode_res_url = omp_hode_res_url
        self.bruker_url = bruker_url
        self.brukere_batch_url = brukere_batch_url
        self.transaksjon_hode_url = transaksjon_hode_url
        self.transaksjon_linje_url = transaksjon_linje_url
        self.kontoplaner_url = kontoplaner_url
        self.avgiftskoder_url = avgiftskoder_url
        self.ressurs_url = ressurs_url
        self.arbeidsordre_batch_url = arbeidsordre_batch_url
        self.begrepsverdier_batch_url = begrepsverdier_batch_url

    def __repr__(self) -> str:
        return "{cls.__name__}({url!r})".format(cls=type(self), url=self.baseurl)

    @staticmethod
    def _urljoin(base: str, path: Optional[str], *idents: str) -> str:
        if not path:
            path = ""
        return urllib.parse.urljoin(base, path + "/" + "/".join(idents))

    def get_begrepsverdier(self) -> str:
        return self._urljoin(self.baseurl, self.begrepsverdier_url)

    def delete_begrepsverdier(
        self, company_id: str, attribute_id: str, attribute_val: str
    ) -> str:
        return self._urljoin(
            self.baseurl,
            self.begrepsverdier_url,
            str(company_id),
            str(attribute_id),
            str(attribute_val),
        )

    def post_begrepsverdier(self) -> str:
        return self._urljoin(self.baseurl, self.begrepsverdier_url)

    def get_bilagstyper(self) -> str:
        return self._urljoin(self.baseurl, self.bilagstyper_url)

    def delete_bilagstyper(
        self, company_id: str, attribute_id: str, attribute_val: str
    ) -> str:
        return self._urljoin(
            self.baseurl,
            self.bilagstyper_url,
            str(company_id),
            str(attribute_id),
            str(attribute_val),
        )

    def post_bilagstyper(self) -> str:
        return self._urljoin(self.baseurl, self.bilagstyper_url)

    def get_firma(self) -> str:
        return self._urljoin(self.baseurl, self.firma_url)

    def get_periode(self) -> str:
        return self._urljoin(self.baseurl, self.periode_url)

    def post_firma(self) -> str:
        return self._urljoin(self.baseurl, self.firma_url)

    def post_prosjekt(self) -> str:
        return self._urljoin(self.baseurl, self.prosjekt_url)

    def post_konteringsregler(self, firma: str) -> str:
        return self._urljoin(self.baseurl, self.konteringsregler_url, str(firma))

    def post_periode(self) -> str:
        return self._urljoin(self.baseurl, self.periode_url)

    def post_arbeidsordre(self) -> str:
        return self._urljoin(self.baseurl, self.arbeidsordre_url)

    def post_arbeidsordre_batch(self) -> str:
        return self._urljoin(self.baseurl, self.arbeidsordre_batch_url)

    def post_begrepsverdier_batch(self) -> str:
        return self._urljoin(self.baseurl, self.begrepsverdier_batch_url)

    def post_brukere_batch(self, company_id: str) -> str:
        return self._urljoin(self.baseurl, self.brukere_batch_url, str(company_id))

    def post_prosjekt_batch(self) -> str:
        return self._urljoin(self.baseurl, self.prosjekt_batch_url)

    def post_data_from_setra(self, client_code: str, batchid: int) -> str:
        return self._urljoin(
            self.baseurl, self.omp_hode_url, str(client_code), str(batchid)
        )

    def post_ubw_report(self, client_code: str, batchid: int) -> str:
        return self._urljoin(
            self.baseurl, self.omp_hode_res_url, str(client_code), str(batchid)
        )

    def delete_bruker(self, client_code: str, bruker_id: str) -> str:
        return self._urljoin(
            self.baseurl, self.bruker_url, str(client_code), str(bruker_id)
        )

    def post_bruker(self, client_code: str) -> str:
        return self._urljoin(self.baseurl, self.bruker_url, str(client_code))

    def get_transaksjon_hode(self, batchid: int, firma: str) -> str:
        return self._urljoin(
            self.baseurl, self.transaksjon_hode_url, str(batchid), str(firma)
        )

    def get_transaksjon_linje(self, batchid: int, firma: str) -> str:
        return self._urljoin(
            self.baseurl, self.transaksjon_linje_url, str(batchid), str(firma)
        )

    def post_kontoplaner(self, firma: str) -> str:
        return self._urljoin(self.baseurl, self.kontoplaner_url, firma)

    def post_avgiftskoder(self, firma: str) -> str:
        return self._urljoin(self.baseurl, self.avgiftskoder_url, firma)

    def post_ressurs(self) -> str:
        return self._urljoin(self.baseurl, self.ressurs_url)


class OmposteringClient:
    default_headers = {
        "Accept": "application/json",
    }

    # TODO Refactor to take OmposteringEndpoints instead of duplicating it here
    def __init__(
        self,
        url: str,
        begrepsverdier_url: Optional[str] = "begrepsverdier_api/",
        bilagstyper_url: Optional[str] = "bilagstyper_api/",
        firma_url: Optional[str] = "firma_api/",
        prosjekt_url: Optional[str] = "prosjekt_api/",
        prosjekt_batch_url: Optional[str] = "prosjekt_batch_api/",
        konteringsregler_url: Optional[str] = "konteringsregler_api/",
        periode_url: Optional[str] = "perioder_api/",
        arbeidsordre_url: Optional[str] = "arbeidsordre_api/",
        arbeidsordre_batch_url: Optional[str] = "arbeidsordre_batch_api/",
        begrepsverdier_batch_url: Optional[str] = "begrepsverdier_batch_api/",
        omp_hode_url: Optional[str] = "omp_hode_api/",
        omp_hode_res_url: Optional[str] = "omp_hode_res_api/",
        bruker_url: Optional[str] = "bruker_api/",
        brukere_batch_url: Optional[str] = "brukere_batch_api/",
        transaksjon_hode_url: Optional[str] = "transaksjon_hode_api/",
        transaksjon_linje_url: Optional[str] = "transaksjon_linje_api/",
        kontoplaner_url: Optional[str] = "kontoplaner_api/",
        avgiftskoder_url: Optional[str] = "avgiftskoder_api/",
        ressurs_url: Optional[str] = "ressurs_api/",
        tokens: Optional[Dict[str, Any]] = None,
        headers: Optional[Dict[str, Any]] = None,
        return_objects: bool = True,
        use_sessions: bool = True,
        line_limit: int = 10000,
    ):
        """
        Ompostering API client.

        :param str url: Base API URL
        :param str begrepsverdier_url: Relative URL to begrepsverdier API
        :param str bilagstyper_url: Relative URL to bilagstyper API
        :param str firma_url: Relative URL to firma API
        :param str prosjekt_url: Relative URL to prosjekt API
        :param str konteringsregler_url: Relative URL to the konteringsregel API
        :param str arbeidsordre_url: Relative URL to arbeidsordre API
        :param str arbeidsordre_batch_url: Relative URL to arbeidsordre_batch API
        :param str periode_url: Relative URL to periode API
        :param str omp_hode_url: Relative URL to omp_hode API
        :param str omp_hode_res_url: Relative URL to omp_hode_res API
        :param str bruker_url: Relative URL to bruker API
        :param str brukere_batch_url: Relative URL to bruker API
        :param str kontoplaner_url: Relative URL to kontoplaner API
        :param str avgiftskoder_url: Relative URL to avgiftskoder API
        :param str ressurs_url: Relative URL to ressurs API
        :params dict tokens: Tokens for the different APIs
        :param dict headers: Append extra headers to all requests
        :param bool return_objects: Return objects instead of raw JSON
        :param bool use_sessions: Keep HTTP connections alive (default True)
        :param str transaksjon_hode_url: Relative URL to OM transaksjon hode API
        :param str transaksjon_linje_url: Relative URL to OM transaksjon linje API
        :param int line_limit: Limit for how many lines to fetch
        """
        self.urls = OmposteringEndpoints(
            url,
            begrepsverdier_url=begrepsverdier_url,
            bilagstyper_url=bilagstyper_url,
            firma_url=firma_url,
            prosjekt_url=prosjekt_url,
            prosjekt_batch_url=prosjekt_batch_url,
            konteringsregler_url=konteringsregler_url,
            periode_url=periode_url,
            arbeidsordre_url=arbeidsordre_url,
            arbeidsordre_batch_url=arbeidsordre_batch_url,
            begrepsverdier_batch_url=begrepsverdier_batch_url,
            omp_hode_url=omp_hode_url,
            omp_hode_res_url=omp_hode_res_url,
            bruker_url=bruker_url,
            brukere_batch_url=brukere_batch_url,
            transaksjon_hode_url=transaksjon_hode_url,
            transaksjon_linje_url=transaksjon_linje_url,
            kontoplaner_url=kontoplaner_url,
            avgiftskoder_url=avgiftskoder_url,
            ressurs_url=ressurs_url,
        )
        self.tokens = {} if tokens is None else tokens
        self.headers = merge_dicts(self.default_headers, headers)
        self.return_objects = return_objects
        self.line_limit = line_limit

        self.session: Union[ModuleType, requests.Session]
        if use_sessions:
            self.session = requests.Session()
        else:
            self.session = requests

    def _build_request_headers(
        self, headers: Optional[Dict[str, str]]
    ) -> Dict[str, Any]:
        request_headers = {}
        for h in self.headers:
            request_headers[h] = self.headers[h]
        for h in headers or ():
            request_headers[h] = headers[h]  # type: ignore[index]
        return request_headers

    def call(
        self,
        method_name: str,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        params: Union[None, Dict[str, Any], Any] = None,
        return_response: Optional[bool] = True,
        **kwargs: Any,
    ) -> Union[JsonType, requests.Response]:
        headers = self._build_request_headers(headers)
        if params is None:
            params = {}
        logger.debug(
            "Calling %s %s with params=%r",
            method_name,
            urllib.parse.urlparse(url).path,
            params,
        )
        r = self.session.request(
            method_name, url, headers=headers, params=params, **kwargs
        )
        if r.status_code in (500, 400, 401, 404):
            logger.warning("Got HTTP %d: %r for url: %s", r.status_code, r.content, url)
        if return_response:
            return r
        r.raise_for_status()
        return r.json()

    def get(self, url: str, **kwargs: Any) -> JsonType:
        return self.call("GET", url, **kwargs)

    def put(self, url: str, **kwargs: Any) -> JsonType:
        return self.call("PUT", url, **kwargs)

    def delete(self, url: str, **kwargs: Any) -> JsonType:
        return self.call("DELETE", url, **kwargs)

    def post(self, url: str, **kwargs: Any) -> JsonType:
        return self.call("POST", url, **kwargs)

    def object_or_data(
        self, cls: Type[BaseModel_T], data: Dict[str, Any]
    ) -> Union[BaseModel_T, List[Any], Dict[str, Any]]:
        if not self.return_objects:
            return data
        return cls.from_dict(data)

    def object_or_data_list(
        self, cls: Type[BaseModel_T], data: List[Dict[str, Any]]
    ) -> Union[List[BaseModel_T], List[Dict[str, Any]]]:
        if not self.return_objects:
            return data
        return [cls.from_dict(d) for d in data]

    def get_begrepsverdier(
        self,
    ) -> Optional[
        Union[List[Begrepsverdi], Dict[str, Any], List[Dict[str, Any]], None]
    ]:
        url = self.urls.get_begrepsverdier()
        response = self.get(
            url, headers=self.get_headers_with_token("begrepsverdier_api")
        )
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            data = response.json().get("items", [])
            data = [json.loads(d.get("data", {})) for d in data]
            return self.object_or_data_list(Begrepsverdi, data)
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def delete_begrepsverdier(
        self, company_id: str, attribute_id: str, attribute_val: str
    ) -> Any:
        url = self.urls.delete_begrepsverdier(company_id, attribute_id, attribute_val)
        response = self.delete(
            url, headers=self.get_headers_with_token("begrepsverdier_api")
        )
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return response
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_begrepsverdi(self, begrepsverdi: Begrepsverdi) -> Union[str, None]:
        data = begrepsverdi.json(by_alias=True)
        url = self.urls.post_begrepsverdier()
        headers = self.get_headers_with_token(
            "begrepsverdier_api", {"Content-Type": "application/json"}
        )

        response = self.post(url, data=data, headers=headers)
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def get_bilagstyper(
        self,
    ) -> Optional[Union[List[Bilagstype], List[Dict[str, Any]], Dict[str, Any]]]:
        url = self.urls.get_bilagstyper()
        response = self.get(url, headers=self.get_headers_with_token("bilagstyper_api"))
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            data = response.json().get("items", [])
            data = [json.loads(d.get("data", {})) for d in data]
            return self.object_or_data_list(Bilagstype, data)
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def delete_bilagstyper(
        self, company_id: str, attribute_id: str, attribute_val: str
    ) -> Any:
        url = self.urls.delete_bilagstyper(company_id, attribute_id, attribute_val)
        response = self.delete(
            url, headers=self.get_headers_with_token("bilagstyper_api")
        )
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return response
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_bilagstype(self, bilagstype: Bilagstype) -> Union[str, None]:
        data = bilagstype.json(by_alias=True)
        url = self.urls.post_bilagstyper()
        headers = self.get_headers_with_token(
            "bilagstyper_api", {"Content-Type": "application/json"}
        )

        response = self.post(url, data=data, headers=headers)
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def get_firma(
        self,
    ) -> Optional[Union[List[Firma], Dict[str, Any], List[Dict[str, Any]]]]:
        url = self.urls.get_firma()
        response = self.get(url, headers=self.get_headers_with_token("firma_api"))
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            data = response.json().get("items", [])
            data = [json.loads(d.get("data", {})) for d in data]
            return self.object_or_data_list(Firma, data)
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_firma(self, firma: Firma) -> Optional[str]:
        data = firma.json(by_alias=True)
        url = self.urls.post_firma()
        headers = self.get_headers_with_token(
            "firma_api", {"Content-Type": "application/json"}
        )

        response = self.post(url, data=data, headers=headers)
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_arbeidsordre(self, arbeidsordre: Arbeidsordre) -> Optional[str]:
        data = arbeidsordre.json(by_alias=True)
        url = self.urls.post_arbeidsordre()
        headers = {"Content-Type": "application/json"}
        response = self.post(url, data=data, headers=headers)
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_arbeidsordre_batch(
        self, arbeidsordre: typing.Iterable[Arbeidsordre]
    ) -> Optional[str]:
        data = [Arbeidsordre.dict(x, by_alias=True) for x in arbeidsordre]
        if not data:
            raise ValueError("Trying to post empty collection of Arbeidsordre")

        url = self.urls.post_arbeidsordre_batch()
        headers = self.get_headers_with_token(
            "arbeidsordre_batch_api", {"Content-Type": "application/json"}
        )
        response = self.post(url, json=data, headers=headers)
        if response.status_code in (200, 201, 204):
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_begrepsverdier_batch(
        self, begrepsverdi: typing.Iterable[Begrepsverdi]
    ) -> Optional[str]:
        data = [Begrepsverdi.dict(x, by_alias=True) for x in begrepsverdi]
        if not data:
            raise ValueError("Trying to post empty collection of Begrepsverdier")

        url = self.urls.post_begrepsverdier_batch()

        headers = self.get_headers_with_token(
            "begrepsverdier_batch_api",  # Use same api key as for prosjekt_api
            {"Content-Type": "application/json"},
        )

        data_json = json.dumps(data, indent=4, sort_keys=True, default=json_serializer)

        response = self.post(url, data=data_json, headers=headers)
        if response.status_code in (200, 201, 204):
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def get_headers_with_token(
        self, token_id: str, extra: Optional[Dict[str, Any]] = None
    ) -> Dict[str, Any]:
        token = self.tokens.get(token_id, None)
        headers = self.headers
        if token:
            headers.update(token)
        if extra is not None:
            headers.update(extra)
        return headers

    def post_prosjekt(
        self,
        prosjekt: Prosjekt,
    ) -> Any:
        """
        POST Prosjekt to Ompostering-api
        """
        data = prosjekt.json(by_alias=True)
        url = self.urls.post_prosjekt()
        headers = self.get_headers_with_token(
            "prosjekt_api", {"Content-Type": "application/json"}
        )
        response = self.post(url, data=data, headers=headers)
        if response.status_code in (200, 201, 204):
            return response
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_brukere_batch(
        self, company_id: str, brukere: typing.Iterable[Bruker]
    ) -> Optional[str]:
        """Batch of users to omp, company_id should be in form of str (unit4)"""
        data = [Bruker.dict(x, by_alias=True) for x in brukere]
        if not data:
            raise ValueError("Trying to post empty collection of Brukere")

        url = self.urls.post_brukere_batch(company_id)

        headers = self.get_headers_with_token(
            "bruker_api",  # Use same api key as for bruker
            {"Content-Type": "application/json"},
        )

        data_json = json.dumps(data, indent=4, sort_keys=False, default=json_serializer)

        response = self.post(url, data=data_json, headers=headers)
        if response.status_code in (200, 201, 204):
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_prosjekt_batch(
        self, prosjekter: typing.Iterable[Prosjekt]
    ) -> Optional[str]:
        data = [Prosjekt.dict(x, by_alias=True) for x in prosjekter]
        if not data:
            raise ValueError("Trying to post empty collection of Prosjekt")

        url = self.urls.post_prosjekt_batch()

        headers = self.get_headers_with_token(
            "prosjekt_api",  # Use same api key as for prosjekt_api
            {"Content-Type": "application/json"},
        )

        data_json = json.dumps(data, indent=4, sort_keys=True, default=json_serializer)

        response = self.post(url, data=data_json, headers=headers)
        if response.status_code in (200, 201, 204):
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_konteringsregler(
        self,
        firma: str,
        konteringsregler: Konteringsregler,
    ) -> Any:
        """
        POST Konteringsregeler to Ompostering-api
        """
        data = konteringsregler.json(by_alias=True)
        url = self.urls.post_konteringsregler(firma)
        headers = self.get_headers_with_token(
            "konteringsregler_api", {"Content-Type": "application/json"}
        )
        response = self.post(url, data=data, headers=headers)
        if response.status_code in (200, 201, 204):
            return response
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def get_periode(
        self,
    ) -> Optional[Union[List[Periode], List[Dict[str, Any]], Dict[str, Any]]]:
        url = self.urls.get_periode()
        response = self.get(url, headers=self.get_headers_with_token("periode_api"))
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            data = response.json().get("items", [])
            data = [json.loads(d.get("data", {})) for d in data]
            return self.object_or_data_list(Periode, data)
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_periode(self, periode: Periode) -> Any:
        """
        POST Periode to Ompostering-api
        """
        data = periode.json(by_alias=True)
        url = self.urls.post_periode()
        headers = self.get_headers_with_token(
            "periode_api", {"Content-Type": "application/json"}
        )
        response = self.post(url, data=data, headers=headers)
        if response.status_code in (200, 201, 204):
            return response
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def get_omp_hode(
        self, client: str, batch_id: int
    ) -> typing.Union[
        typing.List[Dict[str, Any]], typing.List[OmpHode], OmpHodeError, None
    ]:
        """
        Fetch Omposterings Hode from api

        Useful for fetching the user connected to a specific batch.
        """
        url = self.urls.post_data_from_setra(client, batch_id)
        response = self.get(url, headers=self.get_headers_with_token("omp_hode_url"))
        if 400 <= response.status_code < 600:
            return OmpHodeError(
                batch_id=str(batch_id),
                firma=client,
                error_code=str(response.status_code),
            )
        elif response.status_code == 200:
            data = response.json().get("items", [])
            return self.object_or_data_list(OmpHode, data)
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_setra_data(self, setra_batch: SetraBatch) -> typing.Union[str, None, bool]:
        data = setra_batch.json(by_alias=True)
        url = self.urls.post_data_from_setra(setra_batch.client, setra_batch.batchid)  # type: ignore[arg-type]
        headers = self.get_headers_with_token(
            "omp_hode_url", {"Content-Type": "application/json"}
        )
        response = self.post(url, data=str(data), headers=headers)
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return True
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_ubw_report(
        self, setra_batch: SetraBatch, ubw_report: str
    ) -> typing.Union[bool, None]:
        url = self.urls.post_ubw_report(setra_batch.client, setra_batch.batchid)  # type: ignore[arg-type]
        headers = self.get_headers_with_token(
            "omp_hode_res_url", {"Content-Type": "application/json"}
        )
        if ubw_report is None:
            report = ""
        else:
            report = ubw_report.encode("utf-8")

        response = self.post(
            url,
            data=report,
            headers={
                **headers,
                "Content-type": "text/plain; charset=utf-8",
            },
        )
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return True
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def delete_bruker(self, company_id: str, bruker_id: str) -> Any:
        url = self.urls.delete_bruker(company_id, bruker_id)
        response = self.delete(url, headers=self.get_headers_with_token("bruker_api"))
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return response
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_bruker(self, company_id: str, bruker: Bruker) -> Optional[str]:
        data = bruker.json(by_alias=True)
        url = self.urls.post_bruker(company_id)
        headers = self.get_headers_with_token(
            "bruker_api", {"Content-Type": "application/json"}
        )
        response = self.post(url, data=data, headers=headers)
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def get_transaksjon_hode(
        self, batchid: str, firma: int
    ) -> Union[
        Optional[List[TransaksjonHode]],
        List[Dict[str, Any]],
        Dict[str, Any],
        TransaksjonHodeError,
    ]:
        url = self.urls.get_transaksjon_hode(firma, batchid)
        response = self.get(
            url, headers=self.get_headers_with_token("transaksjon_hode_api")
        )
        if 400 <= response.status_code < 600:
            return TransaksjonHodeError(
                batch_id=batchid,
                firma=str(firma),
                error_code=str(response.status_code),
            )
        elif response.status_code == 200:
            data = response.json().get("items", [])
            return self.object_or_data_list(TransaksjonHode, data)
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def get_transaksjon_linje(
        self, batchid: str, firma: int
    ) -> Optional[Union[Dict[str, Any], List[TransaksjonLinje], List[Dict[str, Any]]]]:
        url = self.urls.get_transaksjon_linje(firma, batchid)
        response = self.get(
            url,
            headers=self.get_headers_with_token("transaksjon_linje_api"),
            params={"limit": self.line_limit},
        )
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            data = response.json().get("items", [])
            return self.object_or_data_list(TransaksjonLinje, data)
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_kontoplaner(self, firma: str, kontoplan: List[Konto]) -> Optional[str]:
        url = self.urls.post_kontoplaner(firma)
        data = Kontoer(__root__=kontoplan).json(by_alias=True)
        headers = self.get_headers_with_token(
            "konto_api", {"Content-Type": "application/json"}
        )

        response = self.post(url, data=data, headers=headers)
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_avgiftskoder(
        self, firma: str, avgiftskoder: List[Avgiftskode]
    ) -> Optional[str]:
        url = self.urls.post_avgiftskoder(firma)
        data = AvgiftskodeListe(__root__=avgiftskoder).json(by_alias=True)
        headers = self.get_headers_with_token(
            "avgiftskoder_api", {"Content-Type": "application/json"}
        )
        response = self.post(url, data=data, headers=headers)
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")

    def post_ressurs(self, ressurs: Ressurs) -> Optional[str]:
        """
        Note that the endpoint does not yet exist in ompostering api.
        At the time this method was written because it was assumed that
        we would need ressurs endpoint. Instead we used begrepsverdier.
        The code for ressurs endpoint was left here in case we might need
        ressurs endpoint in the future.
        """
        data = ressurs.json(by_alias=True)
        url = self.urls.post_ressurs()
        headers = self.get_headers_with_token(
            "ressurs_api", {"Content-Type": "application/json"}
        )
        response = self.post(url, data=data, headers=headers)
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return "Created successfully"
        response.raise_for_status()
        raise ValueError(f"Unhandled response code: {response.status_code}")


def get_client(config_dict: Dict[str, Any]) -> OmposteringClient:
    """
    Get a SapClient from configuration.
    """
    return OmposteringClient(**config_dict)
