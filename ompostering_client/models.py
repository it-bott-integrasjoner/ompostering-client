import datetime
import json
import typing
from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    List,
    Optional,
    Union,
)
import pydantic.generics

NameType = typing.TypeVar("NameType")
BaseModel_T = typing.TypeVar("BaseModel_T", bound="BaseModel")


if typing.TYPE_CHECKING:
    OmposteringDate = datetime.date
else:

    class OmposteringDate(datetime.date):
        @classmethod
        def __get_validators__(cls) -> Iterable[Callable[[str], datetime.datetime]]:
            yield cls.validate_date

        @classmethod
        def validate_date(cls, v: str) -> datetime.datetime:
            return datetime.datetime.strptime(v, "%d.%m.%Y")


def to_lower_camel(s: str) -> str:
    first, *others = s.split("_")
    return "".join([first.lower(), *map(str.capitalize, others)])


class BaseModel(pydantic.BaseModel):
    @classmethod
    def from_dict(cls: typing.Type[BaseModel_T], data: Dict[str, Any]) -> BaseModel_T:
        return cls(**data)

    @classmethod
    def from_json(
        cls: typing.Type[BaseModel_T],
        json_data: Union[str, bytes, bytearray],
    ) -> BaseModel_T:
        data = json.loads(json_data)
        return cls.from_dict(data)


class OmposteringClientConfig(BaseModel):
    url: str
    headers: Optional[Dict[str, Any]] = {}
    tokens: Optional[Dict[str, Dict[str, str]]] = {}
    begrepsverdier_url: Optional[str] = "begrepsverdier_api/"
    konteringsregler_url: Optional[str] = "konteringsregler_api/"
    firma_url: Optional[str] = "firma_api/"
    prosjekt_url: Optional[str] = "prosjekt_api/"
    periode_url: Optional[str] = "perioder_api/"
    arbeidsordre_url: Optional[str] = "arbeidsordre_api/"
    omp_hode_url: Optional[str] = "omp_hode_api/"
    omp_hode_res_url: Optional[str] = "omp_hode_res_api/"
    bruker_url: Optional[str] = "bruker_api/"
    transaksjon_hode_url: Optional[str] = "transaksjon_hode_api/"
    transaksjon_linje_url: Optional[str] = "transaksjon_linje_api/"
    kontoplaner_url: Optional[str] = "kontoplaner_api/"
    avgiftskoder_url: Optional[str] = "avgiftskoder_api/"
    line_limit: Optional[int] = 10000


class UpdatedInfo(BaseModel):
    updated_at: datetime.datetime
    updated_by: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Begrepsverdi(BaseModel):
    attribute_id: str
    attribute_name: str
    attribute_value: str
    company_id: str
    custom_value: float
    description: str
    owner: str
    owner_attribute_id: str
    owner_attribute_name: str
    period_from: int
    period_to: int
    status: str
    last_updated: UpdatedInfo

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Bilagstype(BaseModel):
    company_id: str
    description: str
    status: str
    transaction_series: str
    transaction_type: str
    treatment_code: str
    updated_at: datetime.datetime
    updated_by: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Connection(BaseModel):
    currency_company_id: str
    head_office: str
    legal_acting_company_id: str
    remittance_company_id: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Accounts(BaseModel):
    exchange_gain_account: str
    exchange_loss_account: str
    cost_capitalisation: str
    income_capitalisation_account: str
    reversed_payment_account: str
    undeclared_vat_account: str
    amount_3_balance_account: str
    amount_4_balance_account: str
    balancing_attribute_account: str
    invoice_fee: str
    difference_account: str
    payment_difference_gain: str
    payment_difference_loss: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Currency(BaseModel):
    balance_attribute: str
    currency_split: bool
    currency_code: str
    triangulation_currency: str
    amount3_currency: str
    amount4_currency: str
    currency_type: str
    amount3_difference_account: str
    amount4_difference_account: str
    amount3_max_transaction_difference: int
    amount4_max_transaction_difference: int

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class VAT(BaseModel):
    undecl_vat_for_ap_notes: str
    undecl_vat_for_ar_notes: str
    undecl_vat_for_ap: str
    undecl_vat_for_ar: str
    reverse_vat_when_discount_ap: bool
    reverse_vat_when_discount_ar: bool

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AdditionalInformation(BaseModel):
    contact_person: str
    contact_position: str
    e_mail: str
    e_mail_cc: str
    gtin: str
    url: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AddressInformation(BaseModel):
    country_code: str
    place: str
    postcode: str
    province: str
    street_address: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class PhoneNumbersInformation(BaseModel):
    telephone_1: str
    telephone_2: str
    telephone_3: str
    telephone_4: str
    telephone_5: str
    telephone_6: str
    telephone_7: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class ContactPoint(BaseModel):
    additional_contact_info: AdditionalInformation
    address: AddressInformation
    contact_point_type: str
    last_updated: UpdatedInfo
    phone_numbers: PhoneNumbersInformation
    sort_order: Optional[int]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class RelatedValues(BaseModel):
    unit_value: int
    relation_group: str
    relation_id: str
    relation_name: str
    related_value: str
    percentage: int
    date_from: datetime.datetime
    date_to: datetime.datetime

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Firma(BaseModel):
    company_id: str
    company_name: str
    company_registration_number: str
    country: str
    country_code: str
    current_accounting_period: int
    dim_v2_type: str
    dim_v3_type: str
    employer_id: str
    language_code: str
    maximum_transaction_difference: int
    maximum_payment_difference: int
    municipal: str
    new_company_id: str
    overrun: int
    pay_reference: str
    number_of_periods: int
    remind_reference: str
    system_setup_code: str
    tax_office_name: str
    tax_office_reference: str
    tax_system: str
    vat_registration_number: str
    multi_company_information: Optional[Connection]
    accounting: Optional[Accounts]
    currency_information: Optional[Currency]
    vat_information: Optional[VAT]
    last_updated: UpdatedInfo
    contact_points: Optional[List[ContactPoint]]
    related_values: Optional[List[RelatedValues]]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class DurationInfo(BaseModel):
    created_at: datetime.datetime
    date_from: datetime.datetime
    date_to: datetime.datetime
    completed: datetime.datetime
    timesheet_completion_date: datetime.datetime

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class CustomerInfo(BaseModel):
    currency_code: str
    currency_type: str
    customer_id: str
    external_reference: str
    invoice_level: str
    invoice_separately: bool
    invoice_code: str
    invoice_specification: str
    invoice_status: str
    reference: str
    payment_terms_id: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class InvoiceDetails(BaseModel):
    tax_code: str
    tax_system: str
    invoice_specification: str
    payment_terms_id: str
    invoice_header_text: str
    invoice_footer_text: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Activity(BaseModel):
    ace: str
    activity_Id: str
    activity_date_from: str
    activity_date_to: str
    activity_name: str
    activity_invoice_separately: str
    activity_invoice_code: str
    activity_updated_at: str
    activity_status: str
    activity_updated_by: str
    activity_wbs: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class RelatedValue(BaseModel):
    percentage: int
    date_from: datetime.datetime
    date_to: datetime.datetime
    unit_value: int
    relation_group: str
    relation_id: str
    relation_name: str
    related_value: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Besk(BaseModel):
    pass


class Contract(BaseModel):
    contract_fx: Optional[str]
    date_from_fx: Optional[datetime.datetime]
    date_to_fx: Optional[datetime.datetime]
    received_fx: Optional[str]


class PreAward(BaseModel):
    project_fx: Optional[str]
    costing_seq_fx: Optional[int]
    icosting_seq_fx: Optional[int]


class Styring(BaseModel):
    principle_fx: Optional[str]


class CustomFieldGroups(BaseModel):
    probesk: Besk
    prokontrakt: Contract
    propreaward: PreAward
    prostyring: Styring


class Prosjekt(BaseModel):
    """
    Expected form of projects sent to Ompostering API

    This (and the models this model depend on) is a direct copy of the model
    used by ubw-client: https://git.app.uib.no/it-bott-integrasjoner/ubw-client
    """

    status: str
    wbs: str
    post_time_costs: bool
    company_id: str
    authorisation: str
    has_time_sheet_limit_control: bool
    cost_centre: str
    project_name: str
    category1: str
    category2: str
    category3: str
    category4: str
    main_project: str
    message: str
    authorise_normal_hours: bool
    authorise_overtime: bool
    project_id: str
    project_manager_id: str
    project_type: str
    workflow_state: str
    contains_work_orders: bool
    last_updated: UpdatedInfo
    project_duration: DurationInfo
    customer_information: CustomerInfo
    separate_invoice_details: Optional[InvoiceDetails]
    activities: Optional[List[Activity]]
    contact_points: List[str]
    custom_field_groups: CustomFieldGroups
    related_values: List[RelatedValue]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Konteringsregel(BaseModel):
    account_control: bool
    account_rule: int
    amount3_currency_type: str
    amount_flag: str
    amount_type: str
    calculate_amount3: str
    category1: str
    category1_auto_completion: int
    category1_control: bool
    category1_default_value: str
    category1_user_input: str
    category1_validation_relation: str
    category2: str
    category2_auto_completion: int
    category2_control: bool
    category2_default_value: str
    category2_user_input: str
    category2_validation_relation: str
    category3: str
    category3_auto_completion: int
    category3_control: bool
    category3_default_value: str
    category3_user_input: str
    category3_validation_relation: str
    category4: str
    category4_auto_completion: int
    category4_control: bool
    category4_default_value: str
    category4_user_input: str
    category4_validation_relation: str
    category5: str
    category5_auto_completion: int
    category5_control: bool
    category5_default_value: str
    category5_user_input: str
    category5_validation_relation: str
    category6: str
    category6_auto_completion: int
    category6_control: bool
    category6_default_value: str
    category6_user_input: str
    category6_validation_relation: str
    category7: str
    category7_auto_completion: int
    category7_control: bool
    category7_default_value: str
    category7_user_input: str
    category7_validation_relation: str
    company_id: str
    currency_auto_completion: str
    currency_default_value: str
    currency_user_input: str
    currency_validation_relation: str
    debit_credit_flag: str
    description: str
    number_text: str
    show_misc_text_on_invoice: str
    show_number_text_on_invoice: str
    show_value_text_on_invoice: str
    statistical_value_text: str
    status: str
    tax_code_auto_completion: int
    tax_code_default_value: str
    tax_code_user_input: str
    tax_code_validation_relation: str
    tax_system_auto_completion: int
    tax_system_default_value: str
    tax_system_relation_validation: str
    tax_system_user_input: str
    template_type: str
    updated_at: datetime.datetime
    updated_by: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Konteringsregler(BaseModel):
    __root__: List[Konteringsregel]


class Periode(BaseModel):
    company_id: str
    accounting_period: str
    status: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Arbeidsordre(BaseModel):
    company_id: str
    work_order_id: str
    delprosjekt_fra_dato: str
    delprosjekt_til_dato: str
    regnskapsperiode_fra: str
    regnskapsperiode_til: str
    delprosjekt_statuskode: str
    delprosjekt_navn: str
    delprosjekt_prosjektnr: str
    delprosjekt_koststed: str
    delprosjekt_leder_ansattnr: str
    delprosjekt_finansieringskilde: str
    last_updated_at: str
    last_updated_by: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class SetraTransactionError(BaseModel):
    transaction: str
    validation_error: str
    validation_error_type_code: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class SetraVoucherError(BaseModel):
    voucher: str
    validation_error: str
    validation_error_type_code: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class SetraBatchError(BaseModel):
    date: str
    error: str
    error_type_code: str
    error_level: str
    error_source: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class SetraBatch(BaseModel):
    id: str
    created: str
    batchid_interface: str
    batchid: str
    period: str
    interface: str
    client: str
    vouchertype: str
    batch_validated_ok_date: str
    batch_rejected_code: str
    sent_date: str
    http_response_content: str
    http_response_code: str
    orderno: str
    polling_statuscode: str
    polling_statuscode_date: str
    getresult_date: str
    getresult_statuscode: str
    batch_progress: str
    batch_errors: List[SetraBatchError]
    voucher_errors: List[SetraVoucherError]
    transaction_errors: List[SetraTransactionError]
    voucherno_ubw: Optional[int]
    voucherno_ubw_wflow: Optional[int]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class RoleAndCompany(BaseModel):
    role_id: str
    company_id: str
    person_id: Optional[str]
    resourceConnectionUpdated: Optional[datetime.datetime]
    resourceConnectionUpdatedBy: Optional[str]
    role_connection_valid_from: datetime.datetime
    role_connection_valid_to: datetime.datetime
    role_connection_updated_at: datetime.datetime
    role_connection_status: str
    role_connection_updated_by: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class UserStatus(BaseModel):
    date_from: datetime.datetime
    date_to: datetime.datetime
    status: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Security(BaseModel):
    disabled_until: Optional[datetime.datetime]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Usage(BaseModel):
    is_enabled_for_workflow_process: bool

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Bruker(BaseModel):
    user_id: str
    user_name: str
    description: str
    default_logon_company: str
    user_status: UserStatus
    security: Optional[Security]
    language_code: Optional[str]
    usage: Optional[Usage]
    contact_points: Optional[List[ContactPoint]]
    roles_and_companies: Optional[List[RoleAndCompany]]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class TransaksjonHode(BaseModel):
    periode: str
    batch_id: str
    firma: str
    vouchertype: str
    voucherdate: OmposteringDate
    exref: str
    voucherno_ubw: Optional[int]
    voucherno_ubw_wflow: Optional[int]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class TransaksjonHodeError(BaseModel):
    batch_id: str
    firma: str
    error_code: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class TransaksjonLinje(BaseModel):
    art: str
    sted: Optional[str]
    aktivitet: Optional[str]
    delprosjekt: Optional[str]
    dato: Optional[str]
    belop: float
    beskrivelse: str
    mva: Optional[str]
    ansattnr: Optional[str]
    sequenceno: int

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class ContactPoints(BaseModel):
    pass


class KontoCustomFieldGroups(BaseModel):
    pass


class Konto(BaseModel):
    account: str
    account_group: str
    account_name: str
    account_rule: int
    account_rule_name: str
    account_type: str
    company_id: str
    head_office_account: str
    period_from: int
    period_to: int
    status: str
    last_updated: UpdatedInfo
    contact_points: List[ContactPoints]
    related_values: List[RelatedValue]
    custom_field_groups: KontoCustomFieldGroups

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Kontoer(BaseModel):
    __root__: List[Konto]


class Kontoplan(BaseModel):
    """Made for ease of handling kontoplans

    The ompostering api only accepts a list of Konto but it makes
    things easier if the associated company is kept alongside when handling them
    """

    company_id: str
    accounts: List[Konto]


class PercentageInfo(BaseModel):
    date_from: datetime.datetime
    date_to: datetime.datetime
    factor_vat: int
    status: str
    vat_account: str
    vat_percent: int

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class LinkedTaxCodesInfo(BaseModel):
    linked_tax_code: str
    tax_system: str
    use_base_inclusive_vat: bool

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AvgiftskodeCustomFieldGroups(BaseModel):
    pass


class Avgiftskode(BaseModel):
    allow_amending_calculated_vat: bool
    cash_principle: bool
    company_id: str
    currency_code: str
    description: str
    rounding: int
    rounding_closest: bool
    rounding_downwards: bool
    rounding_upwards: bool
    tax_code: str
    type: str
    update_amount: bool
    used_in_accounts_payable: bool
    used_in_accounts_receivable: bool
    used_in_general_ledger: bool
    use_net_based_calculation: bool
    percentage: PercentageInfo
    last_updated: UpdatedInfo
    linked_tax_codes: Optional[List[LinkedTaxCodesInfo]]
    related_values: List[RelatedValue]
    custom_field_groups: AvgiftskodeCustomFieldGroups

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AvgiftskodeListe(BaseModel):
    __root__: List[Avgiftskode]


class Avgiftskoder(BaseModel):
    """Made for ease of handling avgiftskoder

    The ompostering api only accepts a list of Avgiftskode but it makes
    things easier if the associated company is kept alongside when handling them
    """

    company_id: str
    avgiftskoder: List[Avgiftskode]


class Ressurs(BaseModel):
    """Made for ease of handling ressurser"""

    age: int
    birth_date: datetime.datetime
    company_id: str
    date_from: datetime.datetime
    date_to: datetime.datetime
    language_code: str
    person_id: str
    person_name: str
    personnel_type: str
    status: str
    workflow_state: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class OmpHode(BaseModel):
    bilagsmal: str
    periode: str
    batch_id: int
    vouchertype: str
    exref: str
    voucherdate: OmposteringDate
    firma: str
    bruker: str


class OmpHodeError(BaseModel):
    batch_id: str
    firma: str
    error_code: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True
