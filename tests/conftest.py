import json
import pathlib
from typing import Any

import pytest
import requests_mock

from ompostering_client.client import (
    OmposteringClient,
    OmposteringEndpoints,
)


def load_json_file(name: str) -> Any:
    fixture_file = pathlib.Path(__file__).parent / "fixtures" / name
    with fixture_file.open() as f:
        return json.load(f)


@pytest.fixture
def baseurl():
    return "https://localhost"


@pytest.fixture
def endpoints(baseurl):
    return OmposteringEndpoints(baseurl)


@pytest.fixture
def custom_endpoints(baseurl):
    return OmposteringEndpoints(baseurl, "custom/begrepsverdier/")


@pytest.fixture
def endpoints_without_trailing_slash(baseurl):
    return OmposteringEndpoints(baseurl, "custom/begrepsverdier")


@pytest.fixture
def client(baseurl):
    return OmposteringClient(baseurl)


@pytest.fixture
def mock_api(client):
    with requests_mock.Mocker() as m:
        yield m


@pytest.fixture
def prosjekt_batch():
    return load_json_file("prosjekt_batch.json")


@pytest.fixture
def begrepsverdier():
    return load_json_file("begrepsverdier.json").get("items")[0]


@pytest.fixture
def bilagstyper_data():
    return load_json_file("bilagstyper.json")


@pytest.fixture
def firma():
    return load_json_file("firma.json")


@pytest.fixture
def arbeidsordre_72():
    return load_json_file("arbeidsordre_72.json")


@pytest.fixture
def begrepsverdi_72():
    return load_json_file("begrepsverdi_72.json")


@pytest.fixture
def project():
    return load_json_file("project.json")


@pytest.fixture
def periode():
    return load_json_file("periode.json")


@pytest.fixture
def bruker():
    return load_json_file("bruker.json")


@pytest.fixture
def setra_batch():
    return load_json_file("setra_batch.json")


@pytest.fixture
def transaction_linje():
    return load_json_file("transaction_linje.json")


@pytest.fixture
def avgiftskoder():
    return load_json_file("avgiftskoder.json")


@pytest.fixture
def ressurs():
    return load_json_file("ressurs.json")


@pytest.fixture
def omp_hode():
    return load_json_file("omp_hode.json")


def pytest_collection_modifyitems(config, items):
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)
