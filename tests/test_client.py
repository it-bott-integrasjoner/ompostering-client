import datetime
import typing

import pytest
import requests

from ompostering_client.client import OmposteringClient, get_client
from ompostering_client.models import (
    OmpHode,
    Arbeidsordre,
    Begrepsverdi,
    Bruker,
    Prosjekt,
)


@pytest.fixture
def header_name():
    return "X-Test"


@pytest.fixture
def client_cls(header_name):
    class TestClient(OmposteringClient):
        default_headers = {
            header_name: "6a9a32f0-7322-4ef3-bbce-6685a3388e67",
        }

    return TestClient


def test_init_does_not_mutate_arg(client_cls, baseurl):
    headers: typing.Dict[str, typing.Any] = {}
    client = client_cls(baseurl, headers=headers)
    assert headers is not client.headers
    assert not headers


def test_init_applies_default_headers(client_cls, baseurl, header_name):
    headers: typing.Dict[str, typing.Any] = {}
    client = client_cls(baseurl, headers=headers)
    assert header_name in client.headers
    assert client.headers[header_name] == client.default_headers[header_name]


def test_init_modify_defaults(client_cls, baseurl, header_name):
    headers = {header_name: "ede37fdd-a2ae-4a96-9d80-110528425ea6"}
    client = client_cls(baseurl, headers=headers)
    # Check that we respect the headers arg, and don't use default_headers
    assert client.headers[header_name] == headers[header_name]
    # Check that we don't do this by mutating default_headers
    assert client.default_headers[header_name] != headers[header_name]


def test_client_default_endpoints():
    client = OmposteringClient("https://example.com/base")
    assert client.urls.get_begrepsverdier()
    assert client.urls.delete_begrepsverdier("72", "A0", "5555")
    assert client.urls.post_begrepsverdier()
    assert client.urls.get_firma()
    assert client.urls.get_periode()
    assert client.urls.post_firma()
    assert client.urls.post_periode()
    assert client.urls.post_data_from_setra("0", 0)
    assert client.urls.delete_bruker("72", "5-USER")
    assert client.urls.post_prosjekt()
    assert client.urls.post_arbeidsordre()
    assert client.urls.post_arbeidsordre_batch()
    assert client.urls.post_bilagstyper()
    assert client.urls.post_avgiftskoder("72")
    assert client.urls.post_ressurs()


def test_get_omphode(omp_hode, requests_mock):
    client = get_client({"url": "https://example.com/base/"})
    requests_mock.get("https://example.com/base/omp_hode_api/AB/65", json=omp_hode)
    response = client.get_omp_hode("AB", 65)
    assert isinstance(response, list)
    assert isinstance(response[0], OmpHode)
    assert response[0].bruker == "BRUCE.WAYNE@UIB.NO"
    assert response[0].voucherdate == datetime.datetime(2021, 1, 15)


def test_get_omphode_objectless(omp_hode, requests_mock):
    client = get_client({"url": "https://example.com/base/", "return_objects": False})
    requests_mock.get("https://example.com/base/omp_hode_api/AB/65", json=omp_hode)
    response = client.get_omp_hode("AB", 65)
    assert isinstance(response, list)
    assert isinstance(response[0], dict)
    assert response[0]["bruker"] == "BRUCE.WAYNE@UIB.NO"
    assert response[0]["voucherdate"] == "15.01.2021"


def test_post_arbeidsordre_batch_empty(client):
    with pytest.raises(ValueError):
        client.post_arbeidsordre_batch(())


def test_post_prosjekt_batch_empty(client):
    with pytest.raises(ValueError, match="Trying to post empty collection of Prosjekt"):
        client.post_prosjekt_batch(())


def test_post_prosjekt_batch(client, mock_api, project):
    mock_api.post(client.urls.post_prosjekt_batch(), status_code=200)

    prosjekter = [Prosjekt(**project)]
    res = client.post_prosjekt_batch(prosjekter)

    assert res == "Created successfully"


def test_post_bruker_batch_empty(client):
    with pytest.raises(ValueError, match="Trying to post empty collection of Brukere"):
        client.post_brukere_batch("UL", ())


def test_post_bruker_batch(client, mock_api, bruker):
    mock_api.post(client.urls.post_brukere_batch("UL"), status_code=200)

    brukere = [Bruker(**bruker)]
    res = client.post_brukere_batch("UL", brukere)

    assert res == "Created successfully"


def test_post_arbeidsordre_batch(client, mock_api, arbeidsordre_72):
    mock_api.post(client.urls.post_arbeidsordre_batch(), status_code=200)

    res = client.post_arbeidsordre_batch((Arbeidsordre(**arbeidsordre_72),))

    assert res == "Created successfully"


def test_post_arbeidsordre_batch_404(client, mock_api, arbeidsordre_72):
    mock_api.post(client.urls.post_arbeidsordre_batch(), status_code=404)

    with pytest.raises(requests.HTTPError) as e:
        client.post_arbeidsordre_batch((Arbeidsordre(**arbeidsordre_72),))

    assert e.value.response.status_code == 404


def test_post_begrepsverdier_batch(client, mock_api, begrepsverdi_72):
    mock_api.post(client.urls.post_begrepsverdier_batch(), status_code=200)

    res = client.post_begrepsverdier_batch((Begrepsverdi(**begrepsverdi_72),))

    assert res == "Created successfully"


def test_post_begrepsverdier_batch_404(client, mock_api, begrepsverdi_72):
    mock_api.post(client.urls.post_begrepsverdier_batch(), status_code=404)

    with pytest.raises(requests.HTTPError) as e:
        client.post_begrepsverdier_batch((Begrepsverdi(**begrepsverdi_72),))

    assert e.value.response.status_code == 404
