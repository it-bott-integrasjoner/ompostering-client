from ompostering_client.client import OmposteringEndpoints


def test_init(baseurl):
    endpoints = OmposteringEndpoints(baseurl)
    assert endpoints.baseurl == baseurl


def test_get_begrepsverdier(baseurl, endpoints):
    url = endpoints.get_begrepsverdier()
    assert url == baseurl + "/begrepsverdier_api/"


def test_delete_begrepsverdier(baseurl, endpoints):
    url = endpoints.delete_begrepsverdier("123", "456", "789")
    assert url == baseurl + "/begrepsverdier_api/123/456/789"


def test_post_begrepsverdier(baseurl, endpoints):
    url = endpoints.post_begrepsverdier()
    assert url == baseurl + "/begrepsverdier_api/"


def test_post_bilagstyper(baseurl, endpoints):
    url = endpoints.post_bilagstyper()
    assert url == baseurl + "/bilagstyper_api/"


def test_get_firma(baseurl, endpoints):
    url = endpoints.get_firma()
    assert url == baseurl + "/firma_api/"


def test_post_firma(baseurl, endpoints):
    url = endpoints.post_firma()
    assert url == baseurl + "/firma_api/"


def test_get_periode(baseurl, endpoints):
    url = endpoints.get_periode()
    assert url == baseurl + "/perioder_api/"


def test_post_periode(baseurl, endpoints):
    url = endpoints.post_periode()
    assert url == baseurl + "/perioder_api/"


def test_custom_get_begrepsverdier(custom_endpoints, baseurl):
    assert custom_endpoints.get_begrepsverdier() == baseurl + "/custom/begrepsverdier/"


def test_without_trailing_slash(endpoints_without_trailing_slash, baseurl):
    assert (
        endpoints_without_trailing_slash.get_begrepsverdier()
        == baseurl + "/custom/begrepsverdier/"
    )


def test_post_avgiftskoder(baseurl, endpoints):
    url = endpoints.post_avgiftskoder("72")
    assert url == baseurl + "/avgiftskoder_api/72"


def test_post_ressurs(baseurl, endpoints):
    url = endpoints.post_ressurs()
    assert url == baseurl + "/ressurs_api/"


def test_post_arbeidsordre_batch(baseurl, endpoints):
    url = endpoints.post_arbeidsordre_batch()
    assert url == baseurl + "/arbeidsordre_batch_api/"


def test_post_prosjekt_batch(baseurl, endpoints):
    url = endpoints.post_prosjekt_batch()
    assert url == baseurl + "/prosjekt_batch_api/"


def test_post_bruker_batch_url(baseurl, endpoints):
    company_id = "UL"
    url = endpoints.post_brukere_batch(company_id)
    assert url == baseurl + "/brukere_batch_api/UL"
