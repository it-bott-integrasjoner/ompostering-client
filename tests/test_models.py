from datetime import datetime
from ompostering_client.models import (
    Arbeidsordre,
    Begrepsverdi,
    Bruker,
    Firma,
    OmpHode,
    Periode,
    Prosjekt,
    SetraBatch,
    TransaksjonLinje,
    Bilagstype,
    Avgiftskode,
    Ressurs,
)


def test_begrepsverdi(begrepsverdier):
    data = begrepsverdier
    begrepsverdier = Begrepsverdi.from_dict(data)
    assert begrepsverdier.company_id == data["companyId"]
    assert begrepsverdier.attribute_id == data["attributeId"]
    assert begrepsverdier.attribute_value == data["attributeValue"]


def test_bilagstype(bilagstyper_data):
    bilagstyper_json = bilagstyper_data
    bilagstype_obj = Bilagstype.from_dict(bilagstyper_data[0])
    assert bilagstype_obj.transaction_type == bilagstyper_json[0]["transactionType"]
    assert bilagstype_obj.company_id == bilagstyper_json[0]["companyId"]


def test_firma(firma):
    data = firma
    firma = Firma.from_dict(data)
    assert firma.company_id == data["companyId"]
    assert firma.company_name == data["companyName"]
    assert firma.current_accounting_period == data["currentAccountingPeriod"]

    expected = data["currencyInformation"]["currencyCode"]
    assert firma.currency_information.currency_code == expected


def test_arbeidsordre(arbeidsordre_72):
    data = arbeidsordre_72
    arbeidsordre = Arbeidsordre.from_dict(data)
    assert arbeidsordre.company_id == data["companyId"]
    assert arbeidsordre.work_order_id == data["workOrderId"]
    assert arbeidsordre.delprosjekt_fra_dato == data["delprosjektFraDato"]
    assert arbeidsordre.delprosjekt_til_dato == data["delprosjektTilDato"]
    assert arbeidsordre.regnskapsperiode_fra == data["regnskapsperiodeFra"]
    assert arbeidsordre.regnskapsperiode_til == data["regnskapsperiodeTil"]
    assert arbeidsordre.delprosjekt_statuskode == data["delprosjektStatuskode"]
    assert arbeidsordre.delprosjekt_navn == data["delprosjektNavn"]
    assert arbeidsordre.delprosjekt_prosjektnr == data["delprosjektProsjektnr"]
    assert arbeidsordre.delprosjekt_koststed == data["delprosjektKoststed"]
    assert arbeidsordre.delprosjekt_leder_ansattnr == data["delprosjektLederAnsattnr"]

    expected = data["delprosjektFinansieringskilde"]
    assert arbeidsordre.delprosjekt_finansieringskilde == expected

    assert arbeidsordre.last_updated_at == data["lastUpdatedAt"]
    assert arbeidsordre.last_updated_by == data["lastUpdatedBy"]


def test_periode(periode):
    periode_json = periode
    periode_obj = Periode.from_dict(periode)
    assert periode_obj.company_id == periode_json["companyId"]
    assert periode_obj.accounting_period == periode_json["accountingPeriod"]
    assert periode_obj.status == periode_json["status"]


def test_bruker(bruker):
    bruker_json = bruker
    bruker_obj = Bruker.from_dict(bruker)
    assert bruker_obj.user_id == bruker_json["userId"]
    assert bruker_obj.user_status.status == bruker_json["userStatus"]["status"]


def test_project(project):
    """
    Test root elements of project

    Does not test models used within it, e.g UpdatedInfo
    """
    model = Prosjekt(**project)
    assert model.status == project["status"]
    assert model.wbs == project["wbs"]
    assert model.post_time_costs == project["postTimeCosts"]
    assert model.company_id == project["companyId"]
    assert model.authorisation == project["authorisation"]
    assert model.has_time_sheet_limit_control == (project["hasTimeSheetLimitControl"])
    assert model.cost_centre == project["costCentre"]
    assert model.project_name == project["projectName"]
    assert model.category1 == project["category1"]
    assert model.category2 == project["category2"]
    assert model.category3 == project["category3"]
    assert model.category4 == project["category4"]
    assert model.main_project == project["mainProject"]
    assert model.message == project["message"]
    assert model.authorise_normal_hours == project["authoriseNormalHours"]
    assert model.authorise_overtime == project["authoriseOvertime"]
    assert model.project_id == project["projectId"]
    assert model.project_manager_id == project["projectManagerId"]
    assert model.project_type == project["projectType"]
    assert model.workflow_state == project["workflowState"]
    assert model.contains_work_orders == project["containsWorkOrders"]


def test_setra_batch(setra_batch):
    setra_batch_json = setra_batch
    setra_batch_obj = SetraBatch.from_dict(setra_batch)
    assert setra_batch_obj.created == setra_batch_json["created"]
    assert setra_batch_obj.batchid_interface == setra_batch_json["batchidInterface"]
    assert setra_batch_obj.batchid == setra_batch_json["batchid"]
    assert setra_batch_obj.period == setra_batch_json["period"]
    assert setra_batch_obj.interface == setra_batch_json["interface"]
    assert setra_batch_obj.client == setra_batch_json["client"]
    assert setra_batch_obj.vouchertype == setra_batch_json["vouchertype"]

    expected = setra_batch_json["batchValidatedOkDate"]
    assert setra_batch_obj.batch_validated_ok_date == expected

    assert setra_batch_obj.batch_rejected_code == setra_batch_json["batchRejectedCode"]
    assert setra_batch_obj.sent_date == setra_batch_json["sentDate"]

    expected = setra_batch_json["httpResponseContent"]
    assert setra_batch_obj.http_response_content == expected

    assert setra_batch_obj.http_response_code == setra_batch_json["httpResponseCode"]
    assert setra_batch_obj.orderno == setra_batch_json["orderno"]
    assert setra_batch_obj.polling_statuscode == setra_batch_json["pollingStatuscode"]

    expected = setra_batch_json["pollingStatuscodeDate"]
    assert setra_batch_obj.polling_statuscode_date == expected

    assert setra_batch_obj.getresult_date == setra_batch_json["getresultDate"]

    expected = setra_batch_json["getresultStatuscode"]
    assert setra_batch_obj.getresult_statuscode == expected

    assert setra_batch_obj.batch_progress == setra_batch_json["batchProgress"]


def test_transaction_linje(transaction_linje):
    data = transaction_linje
    transaction_linje = TransaksjonLinje.from_dict(data)
    assert transaction_linje.art == data["art"]
    assert transaction_linje.sted == data["sted"]
    assert transaction_linje.delprosjekt == data["delprosjekt"]
    assert transaction_linje.aktivitet == data["aktivitet"]
    assert transaction_linje.belop == data["belop"]
    assert transaction_linje.beskrivelse == data["beskrivelse"]
    assert transaction_linje.mva == data["mva"]
    assert transaction_linje.ansattnr == data["ansattnr"]
    assert transaction_linje.sequenceno == data["sequenceno"]


def test_avgiftskoder(avgiftskoder):
    data = avgiftskoder
    avgiftskode = Avgiftskode.from_dict(avgiftskoder[0])

    expected = data[0]["allowAmendingCalculatedVat"]
    assert avgiftskode.allow_amending_calculated_vat == expected

    assert avgiftskode.cash_principle == data[0]["cashPrinciple"]
    assert avgiftskode.company_id == data[0]["companyId"]
    assert avgiftskode.currency_code == data[0]["currencyCode"]
    assert avgiftskode.description == data[0]["description"]
    assert avgiftskode.rounding == data[0]["rounding"]
    assert avgiftskode.rounding_closest == data[0]["roundingClosest"]
    assert avgiftskode.rounding_downwards == data[0]["roundingDownwards"]
    assert avgiftskode.rounding_upwards == data[0]["roundingUpwards"]
    assert avgiftskode.tax_code == data[0]["taxCode"]
    assert avgiftskode.type == data[0]["type"]
    assert avgiftskode.update_amount == data[0]["updateAmount"]
    assert avgiftskode.used_in_accounts_payable == data[0]["usedInAccountsPayable"]

    expected = data[0]["usedInAccountsReceivable"]
    assert avgiftskode.used_in_accounts_receivable == expected

    assert avgiftskode.used_in_general_ledger == data[0]["usedInGeneralLedger"]
    assert avgiftskode.use_net_based_calculation == data[0]["useNetBasedCalculation"]

    date = data[0]["percentage"]["dateFrom"]
    expected = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S.%f")
    assert avgiftskode.percentage.date_from == expected

    date = data[0]["percentage"]["dateTo"]
    expected = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S.%f")
    assert avgiftskode.percentage.date_to == expected

    assert avgiftskode.percentage.factor_vat == data[0]["percentage"]["factorVat"]
    assert avgiftskode.percentage.status == data[0]["percentage"]["status"]
    assert avgiftskode.percentage.vat_account == data[0]["percentage"]["vatAccount"]
    assert avgiftskode.percentage.vat_percent == data[0]["percentage"]["vatPercent"]
    date = data[0]["lastUpdated"]["updatedAt"]
    expected = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S.%f")
    assert avgiftskode.last_updated.updated_at == expected

    assert avgiftskode.last_updated.updated_by == data[0]["lastUpdated"]["updatedBy"]


def test_ressurs(ressurs):
    ressurs_json = ressurs
    ressurs_obj = Ressurs.from_dict(ressurs[0])
    assert ressurs_obj.person_id == ressurs_json[0]["personId"]
    assert ressurs_obj.person_name == ressurs_json[0]["personName"]


def test_omp_hode(omp_hode):
    omp_hode = omp_hode["items"][0]
    omp_hode_obj = OmpHode(**omp_hode)
    assert omp_hode_obj.voucherdate == datetime(2021, 1, 15)
